/*!
 * \author Stefan Maier <s.maier@kit.edu>, ETP-Karlsruhe
 * \date Apr 30 2020
 */
#ifndef ARDUINOMICRO_H
#define ARDUINOMICRO_H

#include "Arduino.h"
//#include "PowerSupplyChannel.h"

class Connection;

class ArduinoMicro : public Arduino
{
  public:
    ArduinoMicro(const pugi::xml_node configuration);
    virtual ~ArduinoMicro();
    void  configure();
    float getParameterFloat(std::string parName);
    int   getParameterInt(std::string parName);
    bool  getParameterBool(std::string parName);

    void sendCommand(std::string command);
    void setParameter(std::string parName, float value);
    void setParameter(std::string parName, int value);
    void setParameter(std::string parName, bool value);
    // bool turnOn();
    // bool turnOff();

  private:
    Connection* fConnection;
};

#endif
