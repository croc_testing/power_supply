#include "DeviceHandler.h"
#include "CAENelsFastPS.h"
#include "PowerSupply.h"
#ifdef __CAEN__
#include "CAEN.h"
#endif
#include "ArduinoMicro.h"
#include "Hameg7044.h"
#include "Keithley.h"
#include "RohdeSchwarz.h"
//#include "Keithley2410.h"
#include "IsegSHR.h"
#include "TTi.h"
//#include "itIVCurve.h"
#include <iostream>
#include <sstream>
#include <stdexcept> // std::out_of_range

/*!
************************************************
* DeviceHandler constructor.
************************************************
*/
DeviceHandler::DeviceHandler() {}

/*!
************************************************
* DeviceHandler distructor.
************************************************
*/
DeviceHandler::~DeviceHandler()
{
    for(auto it: fPowerSupplyMap) delete it.second;
    fPowerSupplyMap.clear();

    for(auto it: fArduinoMap) delete it.second;
    fArduinoMap.clear();
}

/*!
************************************************
 * Load xml file.
 \param docPath Path to the xml input file.
 \param doc Pugi document to be uploaded.
 \return Pugi parser result.
************************************************
*/
pugi::xml_parse_result DeviceHandler::loadXml(const std::string& docPath, pugi::xml_document& doc)
{
    pugi::xml_parse_result result = doc.load_file(docPath.c_str());
    return result;
}

/*!
****************************** ******************
 * Load xml file error handler.
 \param docPath Input xml file name.
 \param result Pugi parser result.
 \return False if no error occurred.
************************************************
*/
bool DeviceHandler::loadXmlErrorHandler(const std::string& docPath, const pugi::xml_parse_result& result)
{
    std::cout << "Xml parse results for " << docPath << std::endl << "Parse error: " << result.description() << ", character pos = " << result.offset << std::endl;
    return false;
}

/*!
************************************************
 * Loads configuration from file testFileName
 * of the type described by testType and
 * executes test accordingly. Verbosity
 * is off by defualt but could be switched on.
 \param testFileName Configuration file.
 \param testType String containing the test
 type.
 \param verbose Sets verbosity on/off.
 \return False if something went wrong, true
 otherwise.
************************************************
*/
/*!
************************************************
 * Loads configuration from file testFileName
 * of the type described by testType and
 * executes test accordingly. Verbosity
 * is off by defualt but could be switched on.
 \param testFileName Configuration file.
 \param testType String containing the test
 type.
 \param verbose Sets verbosity on/off.
 \return False if something went wrong, true
 otherwise.
************************************************
*/
// bool DeviceHandler::ExecuteTest(const std::string& testFileName, const
// std::string &testType, bool verbose)
// {
//   pugi::xml_document docConfigTest;
//   pugi::xml_parse_result result =
//   docConfigTest.load_file(testFileName.c_str()); if (!result)
//     return DeviceHandler::LoadXmlErrorHandler(testFileName, result);
//   if (result && verbose)
//     std::cout << "Xml config loaded: " << result.description() << std::endl;
//   pugi::xml_node testNode = docConfigTest.child(testType.c_str());
//   if (!(testType.compare("iv_curve")))
//   {
//     itIVCurve testIV(testNode, verbose);
//     return testIV.Run();
//   }
//   else
//   {
//     std::cout
//       << "Test "
//       << testType
//       << " not definied or implemented!"
//       << std::endl
//       << "Aborting..."
//       << std::endl;
//     return false;
//   }
//   return true;
// }

/*!
************************************************
 * Loads pugi_xml document and from docPath and
 * finds every power supply's settings.
 * Multimeters and undefined models are not considered.
 * Verbosity boolean is set ON by defualt but
 * may be switched on.
 \param docPath Path to configuration file.
 \param docSettings Pugi_xml document.
 \param verbose Sets verbosity on/off.
 \return Settings of all declared power supplies;
 if it's empty then none has been found.
************************************************
*/
void DeviceHandler::readSettings(const std::string& docPath, pugi::xml_document& docSettings, bool verbose)
{
    pugi::xml_parse_result result = DeviceHandler::loadXml(docPath, docSettings);
    if(verbose)
    {
        if(result) { std::cout << "Xml config file loaded: " << result.description() << std::endl; }
        else
        {
            DeviceHandler::loadXmlErrorHandler(docPath, result);
        }
    }
    fDocumentRoot = docSettings.child("Devices");

    // I do not know why but somehow the loop for the Arduinos must be made BEFORE the loop for the power supplies.....  (Stefan Sep 2020)
    for(pugi::xml_node arduino = fDocumentRoot.child("Arduino"); arduino; arduino = arduino.next_sibling("Arduino"))
    {
        std::string inUse = arduino.attribute("InUse").value();
        if(inUse.compare("Yes") != 0 && inUse.compare("yes") != 0) continue;
        std::string id    = arduino.attribute("ID").value();
        std::string model = arduino.attribute("Model").value();

        if(model.compare("ArduinoMicro") == 0) { fArduinoMap.emplace(id, new ArduinoMicro(arduino)); }
        else
        {
            std::stringstream error;
            error << "The Model: " << model
                  << " is not an available arduino and won't be initialized, "
                     "please check the xml configuration file.";
            throw std::runtime_error(error.str());
        }
    }

    for(pugi::xml_node powerSupply = fDocumentRoot.child("PowerSupply"); powerSupply; powerSupply = powerSupply.next_sibling("PowerSupply"))
    {
        std::string inUse = powerSupply.attribute("InUse").value();
        if(inUse.compare("Yes") != 0 && inUse.compare("yes") != 0) continue;
        std::string id    = powerSupply.attribute("ID").value();
        std::string model = powerSupply.attribute("Model").value();
        if(model.compare("CAENelsFastPS") == 0) { fPowerSupplyMap.emplace(id, new CAENelsFastPS(powerSupply)); }
#ifdef __CAEN__
        else if(model.compare("CAEN") == 0)
        {
            fPowerSupplyMap.emplace(id, new CAEN(powerSupply));
        }
#endif
        else if(model.compare("TTi") == 0)
        {
            fPowerSupplyMap.emplace(id, new TTi(powerSupply));
        }
        else if(model.compare("RohdeSchwarz") == 0)
        {
            fPowerSupplyMap.emplace(id, new RohdeSchwarz(powerSupply));
        }
        else if(model.compare("Hameg7044") == 0)
        {
            fPowerSupplyMap.emplace(id, new Hameg7044(powerSupply));
        }
        else if(model.compare("Keithley") == 0)
        {
            fPowerSupplyMap.emplace(id, new Keithley(powerSupply));
        }
        else if(model.compare("IsegSHR4220") == 0)
        {
            fPowerSupplyMap.emplace(id, new IsegSHR(powerSupply));
        }
        // else if (std::strcmp(Model.c_str(), "Keithley2410") == 0) {
        //   ps_map.emplace(ID, new Keithley2410(PSparameters));
        // }
        else
        {
            std::stringstream error;
            error << "The Model: " << model
                  << " is not an available power supply and won't be initialized, "
                     "please check the xml configuration file.";
            throw std::runtime_error(error.str());
        }
    }
    //   std::string id    = powerSupply.attribute("ID").value();
    //   std::string model = powerSupply.attribute("Model").value();
    //   if (verbose) {
    //     std::cout << "Power supply id: " << id << std::endl;
    //     std::cout << "Power supply model: " << model << std::endl;
    //   }

    //   std::string connection = powerSupply.attribute("Connection").value();
    //   if (verbose) {
    //     if (std::strcmp(connection.c_str(), "Serial") == 0) {
    //       std::cout << "Type of connection: " << connection << std::endl;
    //       std::cout << "Serial port: " << powerSupply.attribute("Port").value()
    //       << "\n" << std::endl;
    //     }
    //     else if (std::strcmp(connection.c_str(), "Ethernet") == 0) {
    //       std::cout << "Type of connection: " << connection << std::endl;
    //       std::cout << "IP address: " <<
    //       powerSupply.attribute("IPAddress").value() << std::endl; std::cout <<
    //       "TCP port: " << powerSupply.attribute("Port").value() << "\n" <<
    //       std::endl;
    //     }
    //     else {
    //       std::cout << "Undefined connection\n" << std::endl;
    //     }
    //   }
    // }
}

PowerSupply* DeviceHandler::getPowerSupply(const std::string& id)
{
    if(fPowerSupplyMap.find(id) == fPowerSupplyMap.end()) { throw std::out_of_range("No power supply with id " + id + " has been configured!"); }
    return fPowerSupplyMap.find(id)->second;
}

Arduino* DeviceHandler::getArduino(const std::string& id)
{
    if(fArduinoMap.find(id) == fArduinoMap.end()) { throw std::out_of_range("No arduino with id " + id + " has been configured!"); }
    return fArduinoMap.find(id)->second;
}

/*!
************************************************
 * Loads settings and initializes any power supply
 * which has been declared in it.
 * If the device is defined correctly and the connection
 * with your PC has been successfully estabilshed
 * then both the corresponding pointer and settings
 * are saved in the unordered_map ps_map.
 * Verbosity boolean is set OFF by defualt but
 * may be switched on.
 \param path Configuration file.
 \param verbose Sets verbosity on/off.
 \return Map with all available power supplies;
 if it's empty then check your configuration file.
************************************************
*/
// void DeviceHandler::initialize(void) {
// for (unsigned int i = 0; i < settings.size(); i++) {
//   std::string ID = settings[i].attribute("ID").value();
//   std::string Model = settings[i].attribute("model").value();
//   std::string Connection = settings[i].attribute("connection").value();
//   std::string IP;
//   std::string Port;
//   int TCP;
//   if (std::strcmp(Connection.c_str(), "Serial") == 0) {
//     Port = settings[i].attribute("port").value();
//     IP = "UNSET";
//     TCP = -1;
//   }
//   if (std::strcmp(Connection.c_str(), "Ethernet") == 0) {
//     Port = "UNSET";
//     IP = settings[i].attribute("IPaddress").value();
//     TCP = settings[i].attribute("TCP").as_int();
//   }
//   DeviceHandler::PSpar PSparameters = DeviceHandler::PSpar(ID, Model,
//   Connection, Port, IP, TCP);
//   // if (std::strcmp(Model.c_str(), "TTi") == 0) {
//   //   if (std::strcmp(Connection.c_str(), "Serial") == 0) {
//   //     ps_map.emplace(ID, new TTi_Serial(PSparameters));
//   //   }
//   //   if (std::strcmp(Connection.c_str(), "Ethernet") == 0) {
//   //     ps_map.emplace(ID, new TTi_Ethernet(PSparameters));
//   //   }
//   // }
//   // else if (std::strcmp(Model.c_str(), "CAENelsFastPS") == 0) {
//   //   ps_map.emplace(ID, new CAENelsFastPS(PSparameters));
//   // }
//   if (std::strcmp(Model.c_str(), "CAEN") == 0) {
//     ps_map.emplace(ID, new CAEN(PSparameters));
//   }
//   // else if (std::strcmp(Model.c_str(), "Keithley2410") == 0) {
//   //   ps_map.emplace(ID, new Keithley2410(PSparameters));
//   // }
//   else {
//     std::cout << Model << " is not an available power supply and won't be
//     initialized, please check the xml configuration file" << std::endl;
//     continue;
//   }
// }
// return ps_map;
//}
